# docker-cssource-v34

A dockerized Counter-Strike: Source dedicated server from 2009, just before the big Orange Box update in 2010.

The Orange Box not only broke many existing modifications for the game, but also tightened up security dramatically, for this reason, I do not recommend running this open to the world. If you do, consider not port forwarding RCON, and leaving the non-privileged user configuration intact.

## why?

As mentioned, the Orange Box update in 2010 broke many mods of the time. One of my personal favorites was [Zombie Horde](https://gamebanana.com/tools/440), a human vs (mostly) CPU zombie fleet not too unlike Zombie Riot. There were some key differentiators, such as zombies pooping out power-up pills, interesting ZH-specific maps, and cool zombie effects like ghost corpse zombies with partial transparency who shocked you as they attacked.

In my quest to try to run Zombie Horde on a modern day Counter-Strike: Source, it became obvious this is nearly impossible without just rewriting Zombie Horde from scratch to run on SourceMod or something. In the interest of digital preservation, I want to make an easy-to-run CS:S v34 server that I can pair with my favorite mods of the era, so that I can play these with friends at LAN parties, and even my children who are now old enough to game with me. It is remarkably special to share otherwise lost experiences like these with others!

The files in-repo were sourced from a thread I found at [setti.info](http://css.setti.info/forum/topic/1375-srcds-v34/) (thank you for keeping these links alive!!). If there is a need and the files available, containers for other CS:S server versions can easily be produced.

If you are reading this, I hope this project enables you to re-live some of your favorite pre-2010 Counter-Strike: Source experiences as well. Happy Fragging! :bomb:

## quickstart

```bash
docker run -d --network host -v ./cfg/server.cfg:/app/cstrike/cfg/server.cfg registry.gitlab.com/adrift/games/docker-cssource-v34 ./srcds_run -game cstrike -tickrate 66 +map de_dust2 +sv_lan 1 +mp_dynamicpricing 0
```

### breakdown:

- `--network host`

This removes the Docker networking from confusing you and the dedicated server software. The process will run on your network just like any other process on your computer.

- `registry.gitlab.com/adrift/games/docker-cssource-v34`

This is the image you're running, which happens to be hosted at GitLab, same as the code.

- `-v ./cfg/server.cfg:/app/cstrike/cfg/server.cfg`

This "bind mounts" the default server.cfg in this code repository, into your runtime container's server.cfg path. You are free to exclude this chunk, or modify the server.cfg to your preference and have it become available to your game server at launch when you start the container.

- `./srcds_run -game cstrike -tickrate 66 +map de_dust2 +sv_lan 1 +mp_dynamicpricing 0`

This is the command that's actually run in the container. If you have run source-based servers before you're likely very familiar with start-up strings like this one. Feel free to modify to your heart's content.