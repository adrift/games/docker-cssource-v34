FROM debian:bookworm-slim

RUN dpkg --add-architecture i386 && \
    apt-get update && apt-get install -y \
        ca-certificates \
        lib32gcc-s1 \
        libncurses5:i386 \
        libsdl2-2.0-0:i386 \
        libstdc++6 \
        libstdc++6:i386 \
        locales \
        locales-all \
        tmux && \
    apt-get clean && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

RUN useradd --home /app --gid root --system steam && \
    mkdir --parents /app && \
    chown steam:root -R /app

WORKDIR /app

COPY --chown=steam:root ./bin/cssds-v34-2009-08-26/ /app

RUN chmod +x ./srcds*

USER steam

CMD [ "/bin/bash" ]